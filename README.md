## System info
* php 7.2.6
* postgres 10.4
* rabbitmq 3.7.5

## software
* symfony 4.1
* supervisor 
* doctrine
* phpunit

## installment
* git clone https://gitlab.com/jr.lebedev/iq.git
* sh build.sh

## usage
to send commands use command.sh [command]
* bank:users - list on users with wallet id e.g.: `sh command.sh bank:users` 
* bank:transaction:list [--status=1] list of all transactions [by status] e.g.: `sh command.sh bank:transaction:list --status=2`
    * DECLINED = 2;
    * SUCCESS = 3;
    * HOLD = 4;
* bank:send <(int)sender wallet id> <(int)receiver wallet id> <(int)amount> [--on-hold] e.g.: `sh command.sh bank:send 1 2 500 [--on-hold]`
* bank:transaction:confirm <transaction_id> e.g.: `sh command.sh bank:transaction:confirm affd48bb-3e91-471e-a5c0-d0f35e6bb94f`
* bank:transaction:cancel <transaction_id> e.g.: `sh command.sh bank:transaction:cancel affd48bb-3e91-471e-a5c0-d0f35e6bb94f`

## удалось достичь
* пользователи со счетами :)
* переводы между пользователями
* отложенные тразнакции
* подтверждение / отмена транзакции

## что не успел сделать
* хотел использовать одну очередь на контейнер и маштабировать через docker service, а не через количество консьюмеров
* избавиться от стороннего менеждера очередей supervisor в пользу нативного systemd
* больше тестов
* обработка исключений
* ответ от сервиса с результатом транзакции