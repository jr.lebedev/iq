<?php

namespace App\Tests;

use App\Entity\Client;
use App\Entity\Wallet;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class WalletTest extends TestCase
{
    /** @var RecursiveValidator */
    private $validator;

    protected function setUp()
    {
        $this->validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
    }

    public function testWalletWithNegativeBalance()
    {
        $wallet = new Wallet(new Client('Pasa', 'Pasa@'), -100);
        $errors = $this->validator->validate($wallet);
        $error = $errors->get(0);

        $this->assertGreaterThan(0, $errors->count());
        $this->assertEquals($error->getCode(), GreaterThanOrEqual::TOO_LOW_ERROR);
    }
}
