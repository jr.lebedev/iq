<?php

namespace App\Tests;

use App\Entity\Client;
use App\Entity\Transaction;
use App\Entity\TransactionStatus;
use App\Entity\Wallet;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints\NotEqualTo;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class TransactionTest extends TestCase
{
    /** @var RecursiveValidator */
    private $validator;

    private $sender;

    private $recipient;

    protected function setUp()
    {
        $this->validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $this->sender = new Client('Pasa', 'Pasa@');
        $this->recipient = new Client('Sasa', 'Sasa@');
    }

    public function testBalanceGreaterOrEqualThanZero()
    {
        $amount = 100;
        $senderWallet = new Wallet($this->sender, 50);
        $recipientWallet = new Wallet($this->recipient, 20);

        $transaction = new Transaction(
            Uuid::uuid4()->toString(),
            $senderWallet,
            $recipientWallet,
            $amount,
            new TransactionStatus(TransactionStatus::NEW, 'NEW')
        );
        $transaction->getSender()->withdraw($amount);

        $errors = $this->validator->validate($transaction);
        $error = $errors->get(0);

        $this->assertGreaterThan(0, $errors->count());
        $this->assertEquals($error->getCode(), GreaterThanOrEqual::TOO_LOW_ERROR);
    }

    public function testConstraintOfTransactionBetweenSameWallets()
    {
        $amount = 100;
        $sender = new Client('Pasa', 'Pasa@');
        $senderWallet = new Wallet($sender, 50);

        $transaction = new Transaction(
            Uuid::uuid4()->toString(),
            $senderWallet,
            $senderWallet,
            $amount,
            new TransactionStatus(TransactionStatus::NEW, 'NEW')
        );
        $transaction->getSender()->withdraw($amount);

        $errors = $this->validator->validate($transaction);
        $error = $errors->get(0);

        $this->assertGreaterThan(0, $errors->count());
        $this->assertEquals($error->getCode(), NotEqualTo::IS_EQUAL_ERROR);
    }
}
