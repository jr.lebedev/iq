<?php

declare(strict_types=1);

namespace App\Tests;

use App\Unils\InputMoneyFormatter;
use PHPUnit\Framework\TestCase;

class InputMoneyFormatterTest extends TestCase
{
    private $formatter;

    public function setUp()
    {
        $this->formatter = new InputMoneyFormatter();
    }

    public function testInputWithNoFraction()
    {
        $money = $this->formatter->format('1234');
        $this->assertEquals(123400, $money);
    }

    public function testInputWithOneFraction()
    {
        $money = $this->formatter->format('1234.5');
        $this->assertEquals(123450, $money);
    }

    public function testInputWithFractionEndingOnZero()
    {
        $money = $this->formatter->format('1234.50');
        $this->assertEquals(123450, $money);
    }

    public function testInputWithFractionEndingTwoZeroes()
    {
        $money = $this->formatter->format('1234.00');
        $this->assertEquals(123400, $money);
    }

    public function testInputWithTwoFractions()
    {
        $money = $this->formatter->format('1234.52');
        $this->assertEquals(123452, $money);
    }

    public function testInputWithMoreThanTwoFractions()
    {
        $money = $this->formatter->format('1234.5555');
        $this->assertEquals(123455, $money);
    }

    public function testInputWithWithSpacesAsSeparator()
    {
        $money = $this->formatter->format('5 500');
        $this->assertEquals(550000, $money);
    }

    public function testInputWithCommaAsFractionSeparator()
    {
        $money = $this->formatter->format('1234,5');
        $this->assertEquals(123450, $money);
    }

    public function testInputWithCommaAsFractionSeparatorAndTwoFractions()
    {
        $money = $this->formatter->format('1234,50');
        $this->assertEquals(123450, $money);
    }
}
