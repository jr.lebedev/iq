<?php

declare(strict_types=1);

namespace App\Message;

use App\Message\Interfaces\TransactionUpdateMessageInterface;

class TransactionUnHoldMessage implements TransactionUpdateMessageInterface
{
    private $id;

    public function getId() : string
    {
        return $this->id;
    }

    public function setId(string $id) : TransactionUnHoldMessage
    {
        $this->id = $id;

        return $this;
    }
}
