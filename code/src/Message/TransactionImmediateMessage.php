<?php

declare(strict_types=1);

namespace App\Message;

use App\Entity\TransactionStatus;
use App\Message\Interfaces\TransactionCreateMessageInterface;

class TransactionImmediateMessage extends TransactionMessageAbstract implements TransactionCreateMessageInterface
{
    public function __construct()
    {
        $this->status = TransactionStatus::NEW;
    }

    public function setId(string $id) : TransactionImmediateMessage
    {
        $this->id = $id;

        return $this;
    }

    public function setSender(int $sender) : TransactionImmediateMessage
    {
        $this->sender = $sender;

        return $this;
    }

    public function setRecipient(int $recipient) : TransactionImmediateMessage
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function setAmount(int $amount) : TransactionImmediateMessage
    {
        $this->amount = $amount;

        return $this;
    }
}