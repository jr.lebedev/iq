<?php

declare(strict_types=1);

namespace App\Message;

abstract class TransactionMessageAbstract
{
    protected $id;

    protected $sender;

    protected $recipient;

    protected $amount;

    protected $status;

    public function getId() : string
    {
        return $this->id;
    }

    public function getSender() : int
    {
        return $this->sender;
    }

    public function getRecipient() : int
    {
        return $this->recipient;
    }

    public function getAmount() : int
    {
        return $this->amount;
    }

    public function getStatus() : int
    {
        return $this->status;
    }
}