<?php

declare(strict_types=1);

namespace App\Message\Interfaces;

interface TransactionUpdateMessageInterface
{
    public function setId(string $id);

    public function getId() : string;
}