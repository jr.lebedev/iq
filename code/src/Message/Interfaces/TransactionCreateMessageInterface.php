<?php

declare(strict_types=1);

namespace App\Message\Interfaces;

interface TransactionCreateMessageInterface
{
    public function getId() : string;

    public function getSender() : int;

    public function getRecipient() : int;

    public function getAmount() : int;

    public function getStatus() : int;
}