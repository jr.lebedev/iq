<?php

declare(strict_types=1);

namespace App\Message;

use App\Entity\TransactionStatus;
use App\Message\Interfaces\TransactionCreateMessageInterface;

class TransactionHoldMessage extends TransactionMessageAbstract implements TransactionCreateMessageInterface
{
    public function __construct()
    {
        $this->status = TransactionStatus::HOLD;
    }

    public function setId(string $id) : TransactionHoldMessage
    {
        $this->id = $id;

        return $this;
    }

    public function setSender(int $sender) : TransactionHoldMessage
    {
        $this->sender = $sender;

        return $this;
    }

    public function setRecipient(int $recipient) : TransactionHoldMessage
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function setAmount(int $amount) : TransactionHoldMessage
    {
        $this->amount = $amount;

        return $this;
    }
}