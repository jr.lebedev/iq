<?php

declare(strict_types=1);

namespace App\Unils;

class InputMoneyFormatter
{
    public function format(string $input) : int
    {
        $amount = (string)$input;
        $amount = str_replace([' ', ','], ['', '.'], $amount);
        $amoArray = explode('.', $amount);

        $multiplier = 100;
        if (isset($amoArray[1])) {
            $multiplier = (strlen($amoArray[1]) == 1) ? 10 : 1;
            $amount = $amoArray[0] . substr($amoArray[1], 0, 2);
        }

        return (int)($amount * $multiplier);
    }
}