<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Clients extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->persist(new Client('Pasa', 'Pasa@'));
        $manager->persist(new Client('Sasa', 'Sasa@'));
        $manager->persist(new Client('Masa', 'Masa@'));
        $manager->persist(new Client('Dasa', 'Dasa@'));
        $manager->persist(new Client('BANK OF AMERICAS', 'bank@usa.com'));

        $manager->flush();
    }
}
