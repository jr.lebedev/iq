<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Client;
use App\Entity\Wallet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Wallets extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $clients = $manager->getRepository(Client::class)->findAll();

        foreach ($clients as $client) {
            $manager->persist(new Wallet($client, mt_rand(100000, 5000000)));
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            Client::class,
        ];
    }
}
