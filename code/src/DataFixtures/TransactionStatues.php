<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\TransactionStatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TransactionStatues extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $manager->persist(new TransactionStatus(TransactionStatus::NEW, 'New transaction'));
        $manager->persist(new TransactionStatus(TransactionStatus::DECLINED, 'Transaction declined'));
        $manager->persist(new TransactionStatus(TransactionStatus::SUCCESS, 'Transaction success'));
        $manager->persist(new TransactionStatus(TransactionStatus::HOLD, 'Transaction on hold'));

        $manager->flush();
    }
}
