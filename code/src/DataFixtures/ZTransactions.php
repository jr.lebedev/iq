<?php
declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Transaction;
use App\Entity\TransactionStatus;
use App\Entity\Wallet;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Ramsey\Uuid\Uuid;

class ZTransactions extends Fixture
{
    public function load(ObjectManager $manager)
    {
        ///** @var Wallet $wallets[] */
        //$wallets = $manager->getRepository(Wallet::class)->findAll();
        //
        ///** @var TransactionStatus[] $statuses */
        //$statuses = $manager->getRepository(TransactionStatus::class)->findAll();
        //
        //for ($i = 1; $i <= 10; $i++) {
        //    $a = new Transaction(
        //        Uuid::uuid4()->toString(),
        //        $this->randomize($wallets),
        //        $this->randomize($wallets),
        //        mt_rand(35000, 560000),
        //        $this->randomize($statuses),
        //        new \DateTime()
        //    );
        //
        //    $manager->persist($a);
        //}
        //$manager->flush();
    }

    private function randomize($array)
    {
        $count = count($array) - 1;
        return $array[mt_rand(0, $count)];
    }

    public function getDependencies() : array
    {
        return [
            Wallets::class
        ];
    }
}
