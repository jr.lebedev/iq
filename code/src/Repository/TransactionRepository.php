<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Transaction;
use App\Entity\TransactionStatus;
use App\Entity\Wallet;
use App\Exception\BadTransactionDataException;
use App\Exception\DuplicateTransactionException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends ServiceEntityRepository
{
    private $em;

    public function __construct(RegistryInterface $registry)
    {
        $this->em = $this->getEntityManager();
        parent::__construct($registry, Transaction::class);
    }

    /**
     * @param $message
     * @return Transaction
     * @throws BadTransactionDataException
     */
    public function createTransaction($message) : Transaction
    {
        $em = $this->getEntityManager();
        $sender = $em->getRepository(Wallet::class)->find($message->getSender());
        $recipient = $em->getRepository(Wallet::class)->find($message->getRecipient());
        $transactionStatus = $em->getRepository(TransactionStatus::class)
            ->find($message->getStatus());

        if (!$sender || !$recipient || !$transactionStatus) {
            throw new BadTransactionDataException();
        }

        $transaction = new Transaction($message->getId(), $sender, $recipient, $message->getAmount(), $transactionStatus);

        if (!$this->isTransactionUnique($transaction)) {
            throw new DuplicateTransactionException();
        }

        return $transaction;
    }

    public function getTransactionsByStatus(int $status) : array
    {
        $em = $transactionStatus = $this->getEntityManager()->getRepository(Transaction::class);

        return ($status == 0) ? $em->findAll() : $em->findBy(['status' => $status]);
    }

    public function isTransactionUnique(Transaction $transaction) : bool
    {
        $unique = true;
        $em = $this->getEntityManager();

        try {
            $em->persist($transaction);
            $em->flush();
        } catch (UniqueConstraintViolationException $e) {
            /* TODO log duplicate transaction */
            $unique = false;
        }

        return $unique;
    }
}
