<?php

declare(strict_types=1);

namespace App\Repository\TransactionOperation;

use App\Entity\Transaction;
use App\Repository\TransactionOperation\Interfaces\OperationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class HoldOperation implements OperationInterface
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }
    public function execute(Transaction $transaction)
    {
        $transaction->getSender()->withdraw($transaction->getAmount());
        $errors = $this->validator->validate($transaction);
        if ($errors->count() > 0) { /** TODO :| */
            $transaction->getSender()->enroll($transaction->getAmount());
            $transaction->decline();
        } else {
            $transaction->hold();
        }
    }
}
