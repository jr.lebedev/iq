<?php

declare(strict_types=1);

namespace App\Repository\TransactionOperation;

use App\Entity\Transaction;
use App\Repository\TransactionOperation\Interfaces\OperationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ExecuteOperation extends ServiceEntityRepository
{
    private $manager;

    public function __construct(RegistryInterface $registry, EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        parent::__construct($registry, Transaction::class);
    }

    public function execute(Transaction $transaction, OperationInterface $operation)
    {
        $this->manager->getConnection()->beginTransaction();

        try {
            $operation->execute($transaction);

            $this->manager->persist($transaction);
            $this->manager->flush();
            $this->manager->getConnection()->commit();
        } catch (\Exception $e) {
            /** TODO Log error */
            $this->manager->getConnection()->rollBack();
        }
    }
}
