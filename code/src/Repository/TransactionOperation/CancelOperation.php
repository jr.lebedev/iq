<?php

declare(strict_types=1);

namespace App\Repository\TransactionOperation;

use App\Entity\Transaction;
use App\Repository\TransactionOperation\Interfaces\OperationInterface;

class CancelOperation implements OperationInterface
{
    public function execute(Transaction $transaction)
    {
        $transaction->getSender()->enroll($transaction->getAmount());
        $transaction->decline();
    }
}
