<?php

declare(strict_types=1);

namespace App\Repository\TransactionOperation;

use App\Entity\Transaction;
use App\Repository\TransactionOperation\Interfaces\OperationInterface;

class UnHoldOperation implements OperationInterface
{
    public function execute(Transaction $transaction)
    {
        $transaction->getRecipient()->enroll($transaction->getAmount());
        $transaction->success();
    }
}
