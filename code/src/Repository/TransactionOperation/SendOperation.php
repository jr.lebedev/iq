<?php

declare(strict_types=1);

namespace App\Repository\TransactionOperation;

use App\Entity\Transaction;
use App\Repository\TransactionOperation\Interfaces\OperationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SendOperation implements OperationInterface
{
    private $validator;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function execute(Transaction $transaction)
    {
        $transaction->getSender()->withdraw($transaction->getAmount());
        $transaction->getRecipient()->enroll($transaction->getAmount());
        $errors = $this->validator->validate($transaction);

        if ($errors->count() > 0) { /** TODO Not the best idea */
            $transaction->getSender()->enroll($transaction->getAmount());
            $transaction->getRecipient()->withdraw($transaction->getAmount());
            $transaction->decline();
        } else {
            $transaction->success();
        }
    }
}
