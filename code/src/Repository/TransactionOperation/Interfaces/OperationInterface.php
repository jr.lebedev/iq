<?php

declare(strict_types=1);

namespace App\Repository\TransactionOperation\Interfaces;

use App\Entity\Transaction;

interface OperationInterface
{
    public function execute(Transaction $transaction);
}
