<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TransactionStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TransactionStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransactionStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransactionStatus[]    findAll()
 * @method TransactionStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionStatusRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TransactionStatus::class);
    }
}
