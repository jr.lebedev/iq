<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Transaction;
use App\Repository\TransactionRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BankTransactionCommand extends Command
{
    protected static $defaultName = 'bank:transaction:list';
    /**
     * @var TransactionRepository
     */
    private $transactionRepo;

    public function __construct(?string $name = null, TransactionRepository $transactionRepo)
    {
        parent::__construct($name);
        $this->transactionRepo = $transactionRepo;
    }

    protected function configure()
    {
        $this->setDescription('List of bank transactions')
            ->addOption('status', null, InputOption::VALUE_OPTIONAL, 'Transaction status');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $status = (int)$input->getOption('status') ?: 0;
        $transactions = $this->transactionRepo->getTransactionsByStatus($status);

        $rows = [];
        foreach ($transactions as /** @var Transaction $transaction */ $transaction) {
            $rows[] = [
                $transaction->getId(),
                $transaction->getSender()->getClient()->getName(),
                $transaction->getRecipient()->getClient()->getName(),
                $transaction->getFormattedAmount(),
                $transaction->getStatus()->getName()
            ];
        }

        $io->table(['Transaction ID', 'Sender', 'Recipient', 'Amount', 'Status'], $rows);
    }
}
