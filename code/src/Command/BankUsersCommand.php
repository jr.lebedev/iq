<?php

namespace App\Command;

use App\Repository\ClientRepository;
use App\Repository\WalletRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class BankUsersCommand extends Command
{
    protected static $defaultName = 'bank:users';

    private $walletRepo;
    /**
     * @var ClientRepository
     */
    private $clientRepo;

    public function __construct(?string $name = null, WalletRepository $walletRepo, ClientRepository $clientRepo)
    {
        parent::__construct($name);
        $this->walletRepo = $walletRepo;
        $this->clientRepo = $clientRepo;
    }

    protected function configure()
    {
        $this->setDescription('List of users in the bank');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $clients = $this->clientRepo->findAll();
        $rows = [];
        foreach ($clients as $client) {
            $rows[] = [
                $client->getId(),
                $client->getName(),
                $client->getWallet()->getId(),
                $client->getWallet()->getFormattedBalance()
            ];
        }
        $io->table(['User ID', 'User Name', 'Wallet ID', 'Balance'], $rows);
    }
}
