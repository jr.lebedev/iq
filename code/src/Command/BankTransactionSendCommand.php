<?php

declare(strict_types=1);

namespace App\Command;

use App\Message\TransactionHoldMessage;
use App\Message\TransactionImmediateMessage;
use App\Repository\TransactionRepository;
use App\Repository\TransactionStatusRepository;
use App\Unils\InputMoneyFormatter;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class BankTransactionSendCommand extends Command
{
    protected static $defaultName = 'bank:send';
    /** @var TransactionStatusRepository */
    private $transactionRepo;
    /** @var MessageBusInterface */
    private $bus;

    private $inputMoneyFormatter;

    public function __construct(?string $name = null, TransactionRepository $transactionRepo, MessageBusInterface $bus)
    {
        parent::__construct($name);
        $this->transactionRepo = $transactionRepo;
        $this->bus = $bus;
        $this->inputMoneyFormatter = new InputMoneyFormatter();
    }

    protected function configure()
    {
        $this->setDescription('Send money all over the wold!')
            ->addArgument('sender', InputArgument::REQUIRED, 'Who is sending money')
            ->addArgument('recipient', InputArgument::REQUIRED, 'Who is receiving money')
            ->addArgument('amount', InputArgument::REQUIRED, 'How mush are we sending')
            ->addOption('on-hold',null,InputOption::VALUE_NONE,'Is Transaction On Hold');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * Had to use setters instead of constructor because of
         * problems with denormalisaton of objects with constructor
         *
         * TODO Facade::createMessage($input->getOption('on-hold'));
         */
        if ($input->getOption('on-hold')) {
            $transactionMessage = new TransactionHoldMessage();
        } else {
            $transactionMessage = new TransactionImmediateMessage();
        }
        $amount = $this->inputMoneyFormatter->format((string)$input->getArgument('amount'));
        $transactionMessage->setId(Uuid::uuid4()->toString())
            ->setSender((int)$input->getArgument('sender'))
            ->setRecipient((int)$input->getArgument('recipient'))
            ->setAmount($amount);

        $this->bus->dispatch($transactionMessage);
    }
}
