<?php

declare(strict_types=1);

namespace App\Command;

use App\Message\TransactionUnHoldMessage;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class BankTransactionUnHoldCommand extends Command
{
    protected static $defaultName = 'bank:transaction:confirm';

    /** @var MessageBusInterface */
    private $bus;

    public function __construct(?string $name = null, MessageBusInterface $bus)
    {
        parent::__construct($name);
        $this->bus = $bus;
    }

    protected function configure()
    {
        $this->setDescription('Confirm transaction')
            ->addArgument('transaction-id', InputArgument::REQUIRED, 'Enter transaction id');

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = (new TransactionUnHoldMessage())
            ->setId($input->getArgument('transaction-id'));

        $this->bus->dispatch($message);
    }
}
