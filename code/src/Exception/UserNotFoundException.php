<?php

declare(strict_types=1);

namespace App\Exception;

class UserNotFoundException extends \LogicException
{
    public function __construct($message)
    {
        $this->message = $message;
        $this->code = 4002;
        parent::__construct($this->message, $this->code, null);
    }
}