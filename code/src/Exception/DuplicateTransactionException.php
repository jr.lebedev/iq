<?php

declare(strict_types=1);

namespace App\Exception;


class DuplicateTransactionException extends \LogicException
{
    public function __construct()
    {
        $this->message = 'Transaction already exists';
        $this->code = 4005;
        parent::__construct($this->message, $this->code, null);
    }
}