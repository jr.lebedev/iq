<?php

declare(strict_types=1);

namespace App\Exception;

class BadTransactionDataException extends \LogicException
{
    public function __construct()
    {
        $this->message = 'Bad data sent to transaction';
        $this->code = 4004;
        parent::__construct($this->message, $this->code, null);
    }
}