<?php

declare(strict_types=1);

namespace App\Exception;

class NotEnoughFoundsException extends \LogicException
{
    protected $message;
    protected $code;
    protected $file;
    protected $line;

    public function __construct(string $message)
    {
        $this->message = $message;
        $this->code = 4000;
        parent::__construct($this->message, $this->code, null);
    }
}