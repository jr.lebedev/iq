<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\Transaction;
use App\Entity\TransactionStatus;
use App\Entity\TransactionStatusState;
use Doctrine\ORM\Event\LifecycleEventArgs;

class TransactionSelectListener
{
    public function postLoad(LifecycleEventArgs $args)
    {
        $this->addTransactionStatusState($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->addTransactionStatusState($args);
    }

    private function addTransactionStatusState(LifecycleEventArgs $args)
    {
        /** @var Transaction $entity */
        $entity = $args->getEntity();

        if (!$entity instanceof Transaction) {
            return;
        }

        $entityManager = $args->getEntityManager();
        $statuses = $entityManager->getRepository(TransactionStatus::class)->findAll();
        $entity->setTransactionStatusState(new TransactionStatusState($statuses));
    }
}