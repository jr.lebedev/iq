<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransactionRepository")
 */
class Transaction
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Wallet", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     * @Assert\NotEqualTo(propertyPath="recipient")
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Wallet", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $recipient;

    /**
     * @ORM\Column(type="integer")
     * @Assert\GreaterThan(0)
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TransactionStatus", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /** @var TransactionStatusState */
    private $transactionStatusState;

    public function __construct(
        string $id,
        Wallet $sender,
        Wallet $recipient,
        int $amount,
        TransactionStatus $status
    )
    {
        $this->id = $id;
        $this->sender = $sender;
        $this->recipient = $recipient;
        $this->amount = $amount;
        $this->status = $status;
        $this->createdAt = new DateTime();
    }

    public function getId() : ?string
    {
        return $this->id;
    }

    public function getAmount() : int
    {
        return (int)$this->amount;
    }

    public function getFormattedAmount() : string
    {
        return money_format('%i', $this->amount/100);
    }

    public function getSender() : Wallet
    {
        return $this->sender;
    }

    public function getRecipient() : Wallet
    {
        return $this->recipient;
    }

    public function getStatus() : TransactionStatus
    {
        return $this->status;
    }

    public function success() : void
    {
        $this->status = $this->transactionStatusState->success($this->status);
    }

    public function decline() : void
    {
        $this->status = $this->transactionStatusState->decline($this->status);
    }

    public function hold() : void
    {
        $this->status = $this->transactionStatusState->hold($this->status);
    }

    public function setTransactionStatusState(TransactionStatusState $transactionStatusState) : void
    {
        $this->transactionStatusState = $transactionStatusState;
    }
}
