<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\WalletRepository")
 */
class Wallet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Client", inversedBy="wallet", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\Valid()
     */
    private $client;

    /**
     * @ORM\Column(type="bigint")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $balance;

    public function __construct(Client $client, int $balance)
    {
        $this->client = $client;
        $this->balance = $balance;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function getBalance(): int
    {
        return (int)$this->balance;
    }

    public function getFormattedBalance()
    {
        return money_format('%i', $this->balance/100);
    }

    public function withdraw(int $amount) : void
    {
        $this->balance -= $amount;
    }

    public function enroll(int $amount) : void
    {
        $this->balance += $amount;
    }
}
