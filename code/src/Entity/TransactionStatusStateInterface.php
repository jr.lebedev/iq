<?php

declare(strict_types=1);

namespace App\Entity;

interface TransactionStatusStateInterface
{
    public function success(TransactionStatus $status) : TransactionStatus;

    public function decline(TransactionStatus $status) : TransactionStatus;

    public function hold(TransactionStatus $status) : TransactionStatus;
}