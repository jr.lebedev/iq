<?php

declare(strict_types=1);

namespace App\Entity;

class TransactionStatusState implements TransactionStatusStateInterface
{
    /** @var TransactionStatus[] */
    private $statuses;

    public function __construct(array $statuses)
    {
        $this->statuses = $statuses;
    }

    public function success(TransactionStatus $status): TransactionStatus
    {
        if ($status->getId() !== TransactionStatus::DECLINED) {
            return $this->find(TransactionStatus::SUCCESS);
        }

        throw new \LogicException('Cannot change status');
    }

    public function decline(TransactionStatus $status): TransactionStatus
    {
        if ($status->getId() !== TransactionStatus::SUCCESS) {
            return $this->find(TransactionStatus::DECLINED);
        }

        throw new \LogicException('Cannot change status');
    }

    public function hold(TransactionStatus $status): TransactionStatus
    {
        if ($status->getId() !== TransactionStatus::NEW) {
            return $this->find(TransactionStatus::HOLD);
        }

        throw new \LogicException('Cannot change status');
    }

    private function find(int $id) : TransactionStatus
    {
        foreach($this->statuses as $status) {
            if ($status->getId() == $id) {
                return $status;
            };
        }

        throw new \LogicException(sprintf('Status %s not found', $id));
    }

}