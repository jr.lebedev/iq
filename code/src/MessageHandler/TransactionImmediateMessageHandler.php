<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Entity\Transaction;
use App\Message\TransactionImmediateMessage;
use App\Repository\TransactionOperation\ExecuteOperation;
use App\Repository\TransactionOperation\SendOperation;
use App\Repository\TransactionRepository;
use App\Repository\TransactionStatusRepository;

class TransactionImmediateMessageHandler extends AbstractTransactionMessageHandler
{
    protected $operation;

    public function __construct(TransactionRepository $tr, ExecuteOperation $c, TransactionStatusRepository $sr, SendOperation $operation)
    {
        $this->operation = $operation;
        parent::__construct($tr, $sr, $c);
    }

    public function __invoke(TransactionImmediateMessage $message) : void
    {
        $this->handle($message);
    }

    protected function getTransaction($message) : Transaction
    {
        return $this->transactionRepo->createTransaction($message);
    }
}