<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Entity\Transaction;
use App\Repository\TransactionOperation\ExecuteOperation;
use App\Repository\TransactionRepository;
use App\Repository\TransactionStatusRepository;

abstract class AbstractTransactionMessageHandler
{
    protected $transactionRepo;
    protected $statusRepo;
    protected $command;
    protected $message;
    protected $operation;

    public function __construct(TransactionRepository $tr, TransactionStatusRepository $sr,ExecuteOperation $c)
    {
        $this->transactionRepo = $tr;
        $this->statusRepo = $sr;
        $this->command = $c;
    }

    protected function handle($message)
    {
        try {
            $transaction = $this->getTransaction($message);
            $this->command->execute($transaction, $this->operation);
        } catch (\Exception $e) {
            // TODO log all
        }
    }

    abstract protected function getTransaction($message) : Transaction;
}