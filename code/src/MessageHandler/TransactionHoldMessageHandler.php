<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Entity\Transaction;
use App\Exception\BadTransactionDataException;
use App\Message\TransactionHoldMessage;
use App\Repository\TransactionOperation\ExecuteOperation;
use App\Repository\TransactionOperation\HoldOperation;
use App\Repository\TransactionRepository;
use App\Repository\TransactionStatusRepository;

class TransactionHoldMessageHandler extends AbstractTransactionMessageHandler
{
    protected $operation;

    public function __construct(TransactionRepository $tr, ExecuteOperation $c, TransactionStatusRepository $sr, HoldOperation $operation)
    {
        $this->operation = $operation;
        parent::__construct($tr, $sr, $c);
    }

    public function __invoke(TransactionHoldMessage $message)
    {
        $this->handle($message);
    }

    protected function getTransaction($message) : Transaction
    {
        return $this->transactionRepo->createTransaction($message);
    }
}