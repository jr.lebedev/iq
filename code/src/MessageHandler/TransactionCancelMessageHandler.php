<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Entity\Transaction;
use App\Message\TransactionCancelMessage;
use App\Repository\TransactionOperation\CancelOperation;
use App\Repository\TransactionOperation\ExecuteOperation;
use App\Repository\TransactionRepository;
use App\Repository\TransactionStatusRepository;

class TransactionCancelMessageHandler extends AbstractTransactionMessageHandler
{
    protected $operation;

    public function __construct(TransactionRepository $tr, ExecuteOperation $c, TransactionStatusRepository $sr, CancelOperation $operation)
    {
        $this->operation = $operation;
        parent::__construct($tr, $sr, $c);
    }

    public function __invoke(TransactionCancelMessage $message)
    {
        $this->handle($message);
    }

    protected function getTransaction($message) : Transaction
    {
        return $this->transactionRepo->find($message->getId());
    }
}