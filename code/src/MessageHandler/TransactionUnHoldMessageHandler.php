<?php

declare(strict_types=1);

namespace App\MessageHandler;

use App\Entity\Transaction;
use App\Message\TransactionUnHoldMessage;
use App\Repository\TransactionOperation\ExecuteOperation;
use App\Repository\TransactionOperation\UnHoldOperation;
use App\Repository\TransactionRepository;
use App\Repository\TransactionStatusRepository;

class TransactionUnHoldMessageHandler extends AbstractTransactionMessageHandler
{
    protected $operation;

    public function __construct(TransactionRepository $tr, ExecuteOperation $c, TransactionStatusRepository $sr, UnHoldOperation $operation)
    {
        $this->operation = $operation;
        parent::__construct($tr, $sr, $c);
    }

    public function __invoke(TransactionUnHoldMessage $message)
    {
        $this->handle($message);
    }

    protected function getTransaction($message) : Transaction
    {
        return $this->transactionRepo->find($message->getId());
    }
}