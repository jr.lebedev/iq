#!/usr/bin/env bash

docker-compose up -d
docker-compose run --rm --no-deps php composer install --dev
docker-compose run --rm --no-deps php cp .env.dist .env
docker-compose run --rm --no-deps php bin/console doctrine:migrations:migrate -n
docker-compose run --rm --no-deps php bin/console doctrine:fixtures:load -n
docker-compose run --rm --no-deps php bin/phpunit
docker-compose exec consumer supervisorctl start consumer:*